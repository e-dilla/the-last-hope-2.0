using UnityEngine;
using System.Collections;
using Deftly;

public class PowerUps : MonoBehaviour {

    public GameObject sheildBall;
    public GameObject emp;
    public bool sheildOn;
    public bool ammoOn;
    public bool speedOn;
    public bool healthOn;
    public Subject player;
    public GameObject SpawnPoint;
    public GameObject FriendlyPrefabs;


    /// <summary>
    /// James Code
    /// </summary>
    public LayerMask Mask;
    public float explosionRadius;
    public float explosionForce;
    public int explosionDamage;
    public GameObject explosionPrefab;


    private GameObject tempGo;
    private Subject tempSub;



	// Use this for initialization
	void Start () {
        sheildBall.SetActive(false);
        
    }
	
	// Update is called once per frame
	void Update () {
	    
        if (sheildOn)
        {
            sheildBall.SetActive(true);
            StartCoroutine(SheildCount());
            player.GodMode = true;
        }

        if (ammoOn)
        {
            player.UnlimitedAmmo = true;
            StartCoroutine(AmmoCount());
        }

        if (speedOn)
        {
            player.ControlStats.MoveSpeed = 15;
            StartCoroutine(SpeedCount());
        }

        if (healthOn)
        {
            player.Stats.Health.Actual += 50;
            healthOn = false;
        }

	}

    IEnumerator SpeedCount()
    {
        yield return new WaitForSeconds(5);
        speedOn = false;
        player.ControlStats.MoveSpeed = 7;
    }

    IEnumerator SheildCount()
    {
        yield return new WaitForSeconds(5);
        sheildOn = false;
        sheildBall.SetActive(false);
        player.GodMode = false;
    }

    IEnumerator AmmoCount()
    {
        yield return new WaitForSeconds(30);
        ammoOn = false;
        player.UnlimitedAmmo = false;
    }

    public void SheildUp()
    {
        sheildOn = true;
    }

    public void AmmoUp()
    {
        ammoOn = true;
    }

    public void SpeedUp()
    {
        speedOn = true;
    }

    public void HealthUp()
    {
        healthOn = true;
    }


    public void EMP() {
        DoDamageAoe();
    }

    public void FriendlyAi()
    {
        FriendlyAiCall();
    }
   
    void FriendlyAiCall()
    {
        StaticUtil.Spawn(FriendlyPrefabs, SpawnPoint.transform.position, SpawnPoint.transform.rotation);
    }


    /// <summary>
    /// Modified AOE code
    /// </summary>
    private void DoDamageAoe() {
        // could use foo.SendMessage, but it is sloppy... Rather pay for GetComponent instead.
        Ray ray = new Ray(transform.position, Vector3.up);
        RaycastHit[] hits = Physics.SphereCastAll(ray, explosionRadius, 0.1f, Mask);
        foreach (RaycastHit thisHit in hits) {
            tempGo = thisHit.collider.gameObject;
            tempSub = tempGo.GetComponent<Subject>();

            if (explosionForce > 0) {
                Rigidbody rb = tempGo.GetComponent<Rigidbody>();
                if (rb != null) rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }

            if (tempGo != null) {
                //0=kinetic which is default damage for guns
                tempSub.DoDamage(explosionDamage, 0, player);
                player.Stats.DamageDealt += explosionDamage;
            }

        }

        if (explosionPrefab != null) StaticUtil.Spawn(explosionPrefab, transform.position, Quaternion.identity);
        tempGo = null;
        tempSub = null;
    }



}
