﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Realm {

    public Material skyBox;
    public GameObject[] envItems;
    public Color ambientColor;
    public float ambientIntensity;

}


public class ZenSkyChange : MonoBehaviour {

    public Realm[] realm;


	// Use this for initialization
	void Start () {

        EnableRealm(0);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}







    public void DisableRealm(int index) {
        if (realm[index].envItems.Length > 0) {
            for (int x = 0; x < realm[index].envItems.Length; x++) {
                realm[index].envItems[x].SetActive(false);
            }
        }
    }

    public void EnableRealm(int index) {
        RenderSettings.skybox = realm[index].skyBox;
        if (realm[index].envItems.Length > 0) {
            for (int x = 0; x < realm[index].envItems.Length; x++) {
                realm[index].envItems[x].SetActive(true);
            }
        }

        RenderSettings.ambientSkyColor = realm[index].ambientColor;
        RenderSettings.ambientIntensity = realm[index].ambientIntensity;
        DynamicGI.UpdateEnvironment();
    }



}
