using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageEnder : MonoBehaviour {

    public GameObject youWinUI;
    //public GameObject youLoseUI;

	// Use this for initialization
	void Start () {
        youWinUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            Time.timeScale = 0;
            youWinUI.SetActive(true);
        }
    }

    public void Continue()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void quit()
    {
        Application.Quit();
    }
}
