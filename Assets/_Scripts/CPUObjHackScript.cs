using UnityEngine;
using System.Collections;

public class CPUObjHackScript : MonoBehaviour
{
    public bool trigger = false;
    public int counter = 6;
    public float timer = 0;
    public GameObject sign;

    void Start()
    {
        sign.SetActive(false);
    }
   

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            trigger = true;
            timer = 0;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            trigger = false;
            timer = 0;
        }
    }

    void Update()
    {

        timer += Time.deltaTime;
        if (timer >= counter && trigger)
        {
            Debug.Log("I did it");
            timer = 0;
            sign.SetActive(true);
        }  
    }

    /*
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            Debug.Log("WTF");
        }
    }

    */
}
