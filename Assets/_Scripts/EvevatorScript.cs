using UnityEngine;
using System.Collections;

public class EvevatorScript : MonoBehaviour {

    public GameObject Hero;
    //public Transform startPoint;

	// Use this for initialization
	void Start () {
        StartCoroutine(heroSpawn());
        //Hero.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        
        StartCoroutine(killMe());
        
        
        
	}

    IEnumerator killMe()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
    
    IEnumerator heroSpawn()
    {
        yield return new WaitForSeconds(2);
        Hero.SetActive(true);
        
    }
    
}
