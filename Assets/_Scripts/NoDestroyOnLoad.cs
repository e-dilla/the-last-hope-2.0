﻿using UnityEngine;
using System.Collections;

public class NoDestroyOnLoad : MonoBehaviour {

    public GameObject[] dontDestroy;

	// Use this for initialization
	void Start () {
        GameObject temp = null;
        
	    for(int x = 0; x < dontDestroy.Length; x++) {
            if (!GameObject.Find(dontDestroy[x].name+"(Clone)")) {
                temp = Instantiate(dontDestroy[x]);
                DontDestroyOnLoad(temp);
            }

        }

	}

}
