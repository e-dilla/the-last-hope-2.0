using UnityEngine;
using System.Collections;
using Deftly;


public class SpawnTiggers : MonoBehaviour {


    public GameObject[] SpawnPoints;
    public GameObject[] SpawnerPrefabs;

    private bool CanSpawn = true;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (CanSpawn)
        {
            if (col.gameObject.name == "Player")
            {
                for (int i = 0; i < SpawnPoints.Length; i++)
                {
                    int b = Random.Range(0, SpawnerPrefabs.Length);
                    StaticUtil.Spawn(SpawnerPrefabs[b], SpawnPoints[i].transform.position, SpawnPoints[i].transform.rotation);
                }

            }
            CanSpawn = false;
            StartCoroutine(SpawnTurnOn());
        } 
    }

    IEnumerator SpawnTurnOn()
    {
        yield return new WaitForSeconds(35);
        CanSpawn = true;
    }
}
