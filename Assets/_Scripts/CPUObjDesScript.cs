using UnityEngine;
using System.Collections;
using Deftly;

public class CPUObjDesScript : MonoBehaviour {

    public Subject CPU;
    private bool point;
    


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        DestoryCPU();
        if (point)
        {
            GameManagerScript.computerNumbers += 1;
        }
        
    }

    void DestoryCPU()
    {
        if (CPU.Stats.Health.Actual <= 0)
        {
            point = true;
            Debug.Log("got one");

        }
    }
}
