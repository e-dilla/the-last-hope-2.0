﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

    private float[] speed;
		

    void Start() {
        Random.seed = (int)Time.time;
        speed = new float[3];
        
        for(int x = 0; x<speed.Length; x++) {
            speed[x] = Random.Range(10, 15);
        }
    }

	// Update is called once per frame
	void Update () {
        
        transform.Rotate(speed[0] * Time.deltaTime, speed[1] * Time.deltaTime, speed[2] * Time.deltaTime);
	}
}
