﻿using UnityEngine;
using System.Collections.Generic;
using Deftly;


[System.Serializable]
public class EnemySpawnPrefab {
    public GameObject enemyPrefab;
    public float spawnChance;
    public bool customSpawnFx = false;
    public GameObject enemySpawnFx;
    public bool customSpawnPoint;
    public SpawnPoint spawnPoint;
    public int oneShotAmount;
}

public class SpawnerSystem : MonoBehaviour {

    /*
     * TODO:
     * Max enemies on screen
     * Spawn types
     * Spawn Options
     * Editor
     *
     *
    */

    /// <summary>
    /// Objective spawns will allow for a limited number of waves before completing
    /// Infinite spawns do as described
    /// OneShot will spawn enemies listed to spawn points then be done and over with
    /// </summary>
    public enum SPAWNTYPES { Objective, Infinite, OneShot };
    public SPAWNTYPES spawnType;

    /// <summary>
    /// Extra wave options
    /// </summary>
    public bool timed;
    /// <summary>
    /// Auto adjust for additional enemies using a given time for a base
    /// </summary>
    public bool autoAdjust;
    public float timeForWave;

    /// <summary>
    /// Universal options
    /// </summary>
    public EnemySpawnPrefab[] enemyPrefabs;
    public int enemyTeamNum;
    public SpawnPoint[] spawnPoints;


    /// <summary>
    /// Objective spawn stuff
    /// </summary>
    public int maxWaves;
    public int enemiesPerWave;
    private int enemiesToAdd;
    [HideInInspector]
    public bool objectiveComplete;
    [HideInInspector]
    public bool objectiveFail;

    /// <summary>
    /// Timer related stuff
    /// </summary>
    private float timer;
    private float timeLimit = 0;
    private bool timeUp;


    /// <summary>
    /// Performance related stuff
    /// </summary>
    public bool maxEnemiesAlive;
    public int maxEnemyNum = 20;

    //temp game stuff
    private Subject[] tempSub;
    private GameObject tempGO;
    private Vector3 tempV3;
    private int tempI;
    private List<GameObject> generated = new List<GameObject>();


    //internal stuff
    private bool canSpawn;
    private int currentWave = 0;
    private bool generateMe;



    // Use this for initialization
    void Start () {
        
        //set a random seed
        Random.seed = System.Environment.TickCount;

        //initialize temp variables
        tempSub = new Subject[maxEnemyNum];
        tempGO = null;
        tempV3 = Vector3.zero;
        tempI = 0;

        canSpawn = true;
        enemiesToAdd = 0;
        generateMe = true;
	}
	
	// Update is called once per frame
	void Update () {

        //Depending on the mode, execute code
        if (canSpawn) {
            switch (spawnType) {
                case SPAWNTYPES.Infinite:
                    if (maxEnemiesAlive) {
                        if (GetEnemiesAlive() < maxEnemyNum) {
                            if (generateMe) {
                                generateMe = false;
                                GenerateWave();
                            }
                            for (int x = 0; x < generated.Count; x++) {
                                if (generated.Count > 0 && GetEnemiesAlive() + 1 <= maxEnemyNum) {
                                    SpawnEnemy(generated[x]);
                                    generated.RemoveAt(x);
                                } else {
                                    break;
                                }
                            }
                            if (generated.Count <= 0) {
                                canSpawn = false;
                            }
                        }
                    } else {
                        GenerateWave();
                        for (int x = 0; x < generated.Count; x++) {
                            SpawnEnemy(generated[x]);
                        }
                        canSpawn = false;
                    }
                    break;
                case SPAWNTYPES.Objective:
                    if (maxEnemiesAlive) {
                        if (GetEnemiesAlive() < maxEnemyNum) {
                            if (generateMe) {
                                generateMe = false;
                                GenerateWave();
                            }
                            for (int x = 0; x < generated.Count; x++) {
                                if (generated.Count > 0 && GetEnemiesAlive()+1 <= maxEnemyNum) {
                                    SpawnEnemy(generated[x]);
                                    generated.RemoveAt(x);
                                } else {
                                    break;
                                }
                            }
                            if(generated.Count <= 0) {
                                canSpawn = false;
                            }
                        }
                    } else {
                        GenerateWave();
                        for(int x = 0; x < generated.Count; x++) {
                            SpawnEnemy(generated[x]);
                        }
                        canSpawn = false;
                    }
                    
                    break;
                case SPAWNTYPES.OneShot:
                    if (maxEnemiesAlive) {
                        if (GetEnemiesAlive() < maxEnemyNum) {
                            for (int x = 0; x < enemyPrefabs.Length; x++) {
                                for (int y = 0; y < enemyPrefabs[x].oneShotAmount; y++) {
                                    if (enemyPrefabs[x].customSpawnPoint) {
                                        SpawnEnemy(enemyPrefabs[x].enemyPrefab, enemyPrefabs[x].spawnPoint);
                                    } else {
                                        SpawnEnemy(enemyPrefabs[x].enemyPrefab);
                                    }
                                }
                            }
                        }
                    } else {
                        for (int x = 0; x < enemyPrefabs.Length; x++) {
                            for (int y = 0; y < enemyPrefabs[x].oneShotAmount; y++) {
                                if (enemyPrefabs[x].customSpawnPoint) {
                                    SpawnEnemy(enemyPrefabs[x].enemyPrefab, enemyPrefabs[x].spawnPoint);
                                } else {
                                    SpawnEnemy(enemyPrefabs[x].enemyPrefab);
                                }
                            }
                        }
                        canSpawn = false;
                    }
                    
                    break;
            }
        }

        if (timed) {
            timeUp = UpdateTimer();
            if (timeUp) {
                objectiveComplete = true;
                objectiveFail = true;
            }
        }

        if(GetEnemiesAlive() == 0 && spawnType != SPAWNTYPES.OneShot) {
            if(spawnType != SPAWNTYPES.Infinite && currentWave >= maxWaves) {
                objectiveComplete = true;
            } else {
                canSpawn = true;
                if (!generateMe) {
                    generateMe = true;
                }
            }
        }
	
	}







    private void SpawnEnemy(GameObject enemy) {
        tempGO = spawnPoints[Random.Range(0, spawnPoints.Length)].gameObject;
        tempV3 = tempGO.transform.position + Random.insideUnitSphere * tempGO.GetComponent<SpawnPoint>().spawnRadius;
        //PoolManager.Pools[poolName].Spawn(enemy.enemyPrefab.transform, tempV3, tempGO.transform.rotation);
        Instantiate(enemy, tempV3, tempGO.transform.rotation);
    }

    private void SpawnEnemy(GameObject enemy, SpawnPoint point) {
        tempV3 = point.gameObject.transform.position + Random.insideUnitSphere * point.spawnRadius;
        Instantiate(enemy, tempV3, point.gameObject.transform.rotation);
    }

    
    private bool UpdateTimer() {
        timer += Time.deltaTime;
        return timer >= timeLimit;
    }


    private int GetEnemiesAlive() {
        tempSub = GameObject.FindObjectsOfType<Subject>();
        int num = 0;

        for(int x = 0; x < tempSub.Length; x++) {
            if (tempSub[x].Stats.TeamId == enemyTeamNum && !tempSub[x].IsDead) {
                num++;
            }
        }
        return num;
    }


    private void GenerateWave() {
        generated.Clear();
        currentWave++;
        if (autoAdjust) {
            enemiesToAdd += Mathf.RoundToInt(enemiesPerWave / 2);
        }

        for(int x = 0; x < (enemiesPerWave+enemiesToAdd); x++) {
            tempI = Random.Range(0, enemyPrefabs.Length);
            generated.Add(enemyPrefabs[tempI].enemyPrefab);
        }
    }


}
