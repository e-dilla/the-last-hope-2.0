using UnityEngine;
using System.Collections;
using Deftly;

public class barrelExplode : MonoBehaviour {

    /// <summary>
    /// James Code
    /// </summary>
    public LayerMask Mask;
    public float explosionRadius;
    public float explosionForce;
    public int explosionDamage;
    public GameObject explosionPrefab;


    private GameObject tempGo;
    private Subject tempSub;
    public Subject player;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "e20m")
        {
            DoDamageAoe();
            Debug.Log("explode");
            Destroy(gameObject);
        }
    }

    private void DoDamageAoe()
    {
        // could use foo.SendMessage, but it is sloppy... Rather pay for GetComponent instead.
        Ray ray = new Ray(transform.position, Vector3.up);
        RaycastHit[] hits = Physics.SphereCastAll(ray, explosionRadius, 0.1f, Mask);
        foreach (RaycastHit thisHit in hits)
        {
            tempGo = thisHit.collider.gameObject;
            tempSub = tempGo.GetComponent<Subject>();

            if (explosionForce > 0)
            {
                Rigidbody rb = tempGo.GetComponent<Rigidbody>();
                if (rb != null) rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }

            if (tempGo != null)
            {
                //0=kinetic which is default damage for guns
                tempSub.DoDamage(explosionDamage, 0, player);
                player.Stats.DamageDealt += explosionDamage;
            }

        }

        if (explosionPrefab != null) StaticUtil.Spawn(explosionPrefab, transform.position, Quaternion.identity);
        tempGo = null;
        tempSub = null;
    }
}
