﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponButtonIndex : MonoBehaviour {
    [HideInInspector]
    public int index;
    private WeaponWheel master;


    void Awake() {
        //For some reason Unity clears out the onClick() action assigned to a button on a prefab so this is to remake it
        UnityEngine.Events.UnityAction WeaponSelectAction = () => { Button_WeaponSelect(); };

        GetComponent<Button>().onClick.AddListener(WeaponSelectAction);
    }

    void Start() {
        master = GameObject.FindGameObjectWithTag("WeaponWheel").GetComponent<WeaponWheel>();
    }

    void Button_WeaponSelect() {
        // master.index = index;
        //master.hideButtons = true;
        master.Button_WeaponSelect(index);
        
    }
}
