﻿using UnityEngine;
using UnityEngine.UI;
using Deftly;
using System.Collections;

public class WeaponWheel : MonoBehaviour {

    //character
    public Subject playerSubject;
    public int numberOfGuns;

    //positional stuff
    public Vector2 center;
    public float radius = 125;
    public float speed;

    [HideInInspector]
    public int index;
    [HideInInspector]
    public bool hideButtons;

    //UI elements
    public Button centerButton;
    public GameObject buttonPrefab;
    private GameObject[] buttons;


    //calculation stuff
    private int ringCount;
    private Rect centerRect;
    private Rect[] ringRect;
    private float angle;
    private bool showButtons;
    private Vector3 hidePos = new Vector3(-1000, -1000, -1000);
    private int oldIndex;



    // Use this for initialization
    void Start () {

        //Instantiate everything
        GenerateButtons();

	}
	
	// Update is called once per frame
	void Update () {


        if (showButtons) {
            for (int x = 0; x < buttons.Length; x++) {
                //index = -1;
                Vector3 pos = new Vector3(ringRect[x].x, ringRect[x].y);
                buttons[x].GetComponent<RectTransform>().anchoredPosition3D = Vector3.MoveTowards(buttons[x].GetComponent<RectTransform>().anchoredPosition3D, pos, (speed*100) * Time.deltaTime);

                if(buttons[buttons.Length-1].GetComponent<RectTransform>().anchoredPosition3D == pos) {
                    oldIndex = index;
                    showButtons = false;
                }
            }
        }

        if (hideButtons) {
            if(index != oldIndex) {
                oldIndex = index;
                StartCoroutine(playerSubject.ChangeWeaponToSlot(index));
            }

            for (int x = 0; x < buttons.Length; x++) {
                if (buttons[x].GetComponent<RectTransform>().anchoredPosition3D == Vector3.zero) {
                    buttons[x].GetComponent<RectTransform>().anchoredPosition3D = hidePos;
                } else {
                    buttons[x].GetComponent<RectTransform>().anchoredPosition3D = Vector3.MoveTowards(buttons[x].GetComponent<RectTransform>().anchoredPosition3D, Vector3.zero, (speed * 100) * Time.deltaTime);
                }

                if (buttons[buttons.Length - 1].GetComponent<RectTransform>().anchoredPosition3D == hidePos) {
                    hideButtons = false;
                }
            }
        }


    }



    public void Button_Press() {

        if (!showButtons && buttons[0].GetComponent<RectTransform>().anchoredPosition3D == hidePos) {
            showButtons = true;
            for (int x = 0; x < buttons.Length; x++) {
                buttons[x].GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
            }
        } else {
            hideButtons = true;
        }

    }

    public void Button_WeaponSelect(int thing) {
        showButtons = false;
        hideButtons = true;
        index = thing;

    }



    private void CountUpdate() {
        //get the angle to position the buttons needed based on count
        ringCount = buttons.Length;
        angle = 360.0f / ringCount;

        //get the corners of the center button
        Vector3[] corners = new Vector3[4];
        centerButton.GetComponent<RectTransform>().GetLocalCorners(corners);

        //get the center width and height for start position to wrap in a circle
        centerRect.height = corners[2].y - corners[0].y;
        centerRect.width = corners[2].x - corners[0].x;
        centerRect.x = center.x - centerRect.width * 0.5f;
        centerRect.y = center.y - centerRect.height * 0.5f;

        ringRect = new Rect[ringCount];   

        //get weapon button width and height
        buttons[0].GetComponent<RectTransform>().GetWorldCorners(corners);
        float w = corners[2].x - corners[0].x;
        float h = corners[2].y - corners[0].y;
        Rect rect = new Rect(0, 0, w, h);

        Vector2 v = new Vector2(radius, 0);

        //calculate the positions for the buttons
        for (int x = 0; x < ringCount; x++) {
            rect.x = center.x + v.x;
            rect.y = center.y + v.y;
            ringRect[x] = rect;
            v = Quaternion.AngleAxis(angle, Vector3.forward) * v;   
        }


    }


    private void GenerateButtons() {
        buttons = new GameObject[numberOfGuns];
       
        for (int x = 0; x < numberOfGuns; x++) {
            buttons[x] = Instantiate(buttonPrefab);
            buttons[x].transform.SetParent(this.gameObject.transform.parent.transform);
            buttons[x].GetComponent<RectTransform>().anchoredPosition3D = hidePos;
           
            buttons[x].GetComponent<WeaponButtonIndex>().index = x;

            buttons[x].transform.GetChild(0).GetComponent<Image>().sprite = playerSubject.WeaponListEditor[x].gameObject.GetComponent<Weapon>().Stats.UiImage;

        }

        
        //Set positions of everything
        CountUpdate();
    }


}
