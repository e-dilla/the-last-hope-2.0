﻿using UnityEngine;
using System.Collections;
using Deftly;

public class ZenTeleporter : MonoBehaviour {


    public GameObject endPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerEnter(Collider other) {
        if (other.GetComponent<Subject>()) {
            //other.transform.position = endPoint.transform.position;
            other.GetComponent<NavMeshAgent>().Warp(endPoint.transform.position);

            //spawn sparkly things
        }
    }
}
