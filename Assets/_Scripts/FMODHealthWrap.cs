﻿using UnityEngine;
using System.Collections;
using Deftly;

public class FMODHealthWrap : MonoBehaviour {

    public FMODUnity.StudioEventEmitter emitter;
    private Subject player;


    // Use this for initialization
    void Start () {
        player = GetComponent<Subject>();

    }
	
	// Update is called once per frame
	void Update () { 
        emitter.SetParameter("health", player.Stats.Health.Actual);
    }
}
