﻿using UnityEngine;
using UnityEditor;




[CustomEditor(typeof(SpawnerSystem)), CanEditMultipleObjects]

public class SpawnSystemEditor : Editor {



    public SerializedProperty
        spawnTypes_Prop,
        timed_Prop,
        autoAdjust_Prop,
        timeForWave_Prop,
        enemyPrefabs_Prop,
        enemyTeamNumber_Prop,
        spawnPoints_Prop,
        maxWaves_Prop,
        maxEnemiesAlive_Prop,
        maxEnemiesNum_Prop,
        enemiesPerWave_Prop;



    // Use this for initialization
    void OnEnable() {
        spawnTypes_Prop = serializedObject.FindProperty("spawnType");
        timed_Prop = serializedObject.FindProperty("timed");
        autoAdjust_Prop = serializedObject.FindProperty("autoAdjust");
        timeForWave_Prop = serializedObject.FindProperty("timeForWave");
        maxWaves_Prop = serializedObject.FindProperty("maxWaves");
        enemyPrefabs_Prop = serializedObject.FindProperty("enemyPrefabs");
        enemyTeamNumber_Prop = serializedObject.FindProperty("enemyTeamNum");
        spawnPoints_Prop = serializedObject.FindProperty("spawnPoints");
        maxEnemiesAlive_Prop = serializedObject.FindProperty("maxEnemiesAlive");
        maxEnemiesNum_Prop = serializedObject.FindProperty("maxEnemyNum");
        enemiesPerWave_Prop = serializedObject.FindProperty("enemiesPerWave");

    }


    public override void OnInspectorGUI() {
        serializedObject.Update();

        //these things are there regardless of what kind of wave it is
        EditorGUILayout.PropertyField(spawnTypes_Prop);
        EditorGUILayout.PropertyField(timed_Prop);
        EditorGUILayout.PropertyField(spawnPoints_Prop, true);

        bool timed = timed_Prop.boolValue;
        if (timed) {
            EditorGUILayout.PropertyField(autoAdjust_Prop);
            EditorGUILayout.PropertyField(timeForWave_Prop);
        }

        EditorGUILayout.PropertyField(enemyPrefabs_Prop, true);
        EditorGUILayout.PropertyField(enemyTeamNumber_Prop);


        //end of list

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();


        //WaveManager.WAVETYPE wt = (WaveManager.WAVETYPE)waveType_Prop.enumValueIndex;
        SpawnerSystem.SPAWNTYPES st = (SpawnerSystem.SPAWNTYPES)spawnTypes_Prop.enumValueIndex;

        //these properties show up depending on what item it is
        switch (st) {
            case SpawnerSystem.SPAWNTYPES.OneShot:

                EditorGUILayout.LabelField(new GUIContent("One Shot Wave Options:"));

                //EditorGUILayout.PropertyField(waveList_Prop, new GUIContent("List of Waves"), true);

                break;
            case SpawnerSystem.SPAWNTYPES.Objective:
                EditorGUILayout.LabelField(new GUIContent("Objective Wave Options:"));
                EditorGUILayout.PropertyField(maxWaves_Prop);
                EditorGUILayout.PropertyField(enemiesPerWave_Prop);
                break;
            case SpawnerSystem.SPAWNTYPES.Infinite:
                EditorGUILayout.LabelField(new GUIContent("Infinite Wave Options:"));
                EditorGUILayout.PropertyField(enemiesPerWave_Prop);
                EditorGUILayout.PropertyField(autoAdjust_Prop);

                break;
        }

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField(new GUIContent("Performance Stuff:"));
        EditorGUILayout.PropertyField(maxEnemiesAlive_Prop);
        EditorGUILayout.PropertyField(maxEnemiesNum_Prop);

        serializedObject.ApplyModifiedProperties();
    }
}
