using UnityEngine;
using System.Collections;
using Deftly;

public class spiderScript : MonoBehaviour {

    Transform Player;
    float moveSpeed = 5f;
    float rotationSpeed = 37f;
    Transform myTransform;
    Subject spider;

	// Use this for initialization
	void Start () {
        myTransform = transform;
        Player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {

        if (spider.Stats.Health.Actual == 0)
        {
            Destroy(this.gameObject);
        }

        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(Player.position - transform.position), rotationSpeed * Time.deltaTime);
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}
}
